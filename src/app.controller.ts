import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('hello')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault() : string {
    return 'Default';
  }

  @Get('hello')
  getHello(): string {
    return '<html><body><h1>Hello Buu</h1></body></html>'
  }

  @Post('world')
  getWorld(): string {
    return '<html><body><h1>Buu World</h1></body></html>'
  }

  @Get('test-query')
  testQuery(
    @Req() req, 
    @Query('celcius') celcius: number, 
    @Query('type') type: string
  ) {
    return {
      celcius: celcius,
      type: type,
    };
  } 

  @Get('test-params/:celcius')
  testParam(@Req() req, @Param('celcius') celcius: number) {
    return {
      celcius
    }
  }

  @Post('test-body')
  testBody(@Req() req, @Body() body, @Body('celcius') celcius: number) {
    return {celcius}
  }
}
